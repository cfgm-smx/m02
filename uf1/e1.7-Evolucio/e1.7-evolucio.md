# E3. Exercici 7. Evolució dels sistemes operatius

## Introducció

El món dels sistemes operatius ha anat evolucionant a partir de principis molt diferents. Els dos grans sistemes originals van ser Windows i UNIX, del qual derivarien Linux i OSX.

## Continguts

Cal visualitzar aquests dos documentals:

[Revolution OS](https://www.youtube.com/watch?v=LgzK3fnJ3iI) (1h26m)
[The Code: Story of Linux documentary](https://www.youtube.com/watch?v=FMTpIi6Dr30) (49m)

## Entrega

1. Què és Unix?

2. Què és GNU? Què volen dir les sigles?

3. Què és Linux?

4. Quan podem considerar que va començar el concepte 'open source'?

5. Què vol dir el concepte 'copy-left'?

6. Què és la llicència GNU i qui la va crear?