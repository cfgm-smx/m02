# Autor:          UF3_E3.0_Grup_Cognom_alumne.md
# Date:           16/04/21
# Description:    E3. Exercici 0. Treball de recerca sobre el SystemD

## 1. Qué és systemD?. Qué és SysV?.
Tradicionalment els sistemes Linux gestionaven els seus serveis amb **SysV** amb un conjunt d'scripts prefeterminats per les distribucions ( Debian, RedHat ...). El sistema es basava en els principis de simplicitat i de gestionar només l'arrencada i aturada de serveis correctament dins d'uns nivells establerts, els runlevels que el definien l'ordre.

Amb **SystemD** es canvia completament la manera de com s'inicien els serveis amb linux, principalment per intentar aportar una millor definició de dependències entre serveis i permetre també més execucions en paral.lel i concurrents, especialment durant l'arrencada del sistema operatiu. 

Des de que en el 2010 un noi a RedHat ho va dissenyar, ara totes les distribucions fan servir **SystemD**.

Per gestionar el serveis ho farem amb l'ordre **systemctl**. Aquesta ordre buscarà el fitxer de configuració del servei per tal de realitzar-ne l'acció corresponent que s'hi indiqui. Tots els serveis poden tenir dependències i això farà que no només s'iniciï el nostre servei sinó també els associats.

## 2. En quin fitxer es troba la configuració general de systemd?
La configuración general de systemd es troba al arxiu; molts valors per defecte estan allà establerts.
```
/etc/systemd/system.conf
```
## 3. Quina comanda fem servir per saber la versió actual de SystemD? 
```
systemctl --version
```

## 4. Que són les units en un systemD?. Fica 3 exemples.
Todo lo que es gestionado por systemd se llama "unit" y cada "unit" es descrita por un archivo de configuración propio, el cual tendrá una extensión diferente según el tipo de de unidad que se trate:  
- .service: Describe la configuración de un demonio ( servicio )  
- .target: Define un grupo de Units (se utiliza a modo de "metapaquete" de Units)  
- .device: Describe los dispositivo gestionados por systemD.  
En aquest mòdul ens basarem solament amb les unit de tipus service.

## 5. Quina informació trobem en els fitxers amb extensió .swap?
Descriu informació sobre particions o arxius d'intercanvi gestionats per systemd. 
Ojo no confondres amb .swp

## 6. Que consultem amb la següent comanda?
##    systemctl list-units --type service
Comprobar totes les unitats del tipus service carregades al sistema

## 7. Quins estats pot tenir una Unit?. Fes una descripció.
Una unidad de servicio puede tener los siguientes estados: 
- enabled: configurada para ejecutarse durante el inicio del sistema. 
- disabled: configurada para no ejecutarse durante el inicio del sistema. 
- active: unidad que se está ejecutando actualmente. 
- inactive: unidad que no se está ejecutando actualmente. 
- failed: unidad que ha fallado durante su inicio. 
Una unidad que no está activada puede estar activa si ha sido arrancada durante la sesión. Del mismo modo, una unidad puede estár inactiva, a pesar de estar activada

State != status
( Ojo, amb fedora tenim --status loaded)
( Ojo, aquí amb fedora ho tenim --status not-found )

## 8. Els arxius de configuració de les Units ( siguin del tipus que siguin ) en quines 3 carpetes poden estar repartides?

Trobem les configuracions que venen per defecte amb els paquets que intal·lem.
- /usr/lib/systemd/system  

Les configuracions que es creen en iniciar el sistema. ( Són més prioritaris que els anterior )  
- /run/systemd/system  

Les configuracions que els administradors configuren. Són els més prioritaris:  
- /etc/systemd/system

## 9. Com podem afegir ( o sobreescriure ) opcions de configuració concretes, sense tenir que tocar les configuracions genèriques de la unit?
Los archivos en /etc/... sobreescriben los archivos homónimos que estén en /run/... 
los cuales sobreescriben los que estén en /usr/lib/... (o en algunas distribuciones, /lib/...).   
Si no tienen el mismo nombre, todos los archivos de las tres carpetas se mezclan ordenados por su nombre de forma numericoalfabètica y se van leyendo en este orden hasta el final.

## 10. Algunes "units" contenen el símbol @ en el seu nom (per exemple, nom@cadena.service); Que significa?
Algunas "units" contienen un símbolo @ en su nombre (por ejemplo, nom@cadena.service); esto significa que son instancias de una unit-plantilla, el archivo de configuración de la cual es el que no contiene la parte "Cadena" en su nombre (así: nombre @.service). La parte "cadena" es el identificador de la instancia (de hecho, dentro del archivo de configuración de la unit-plantilla el valor "cadena" sustituye todas las ocurrencias del especificador especial %i).

## 11. Les següents comandes serveixen per gestionar Units. Descriu el que fa cada una d'elles:
- systemctl --state inactive
Es llisten totes les units inactives del sistema

- systemctl --failed
Es llisten les units que han fallat al iniciar-se

- systemctl --all
Si deseamos mostrar todas las unidades independientemente de su estado, utilizamos el parámetro --all

## 12. Llistar tots els fitxers associats amb els targets
systemctl list-unit-files --type target

## 13. Com reiniciem una Unit?. Posa un exemple per exemple de una Unit de tipus socket
systemctl start nomUnit.socket  
Un socket (enchufe) representa una connexion de red.

## 14. Com sabem si una unit està activada, desactivada o ha fallat?
Ho podem comprovar de moltes maneres. Per exemple:  
```
# Ens dona detall de tots els processos del servei en concret, i per cada un d'ells el seu estat.
systemctl list-unit-files bolt.service  

# Podem veure l'estat d'un servei en concret 
systemctl status bolt.service  

# Per filtrar serveis per estat: active, loaded, not-found, failed
systemctl --state active 
```

## 15. Quina comanda fem servir si volem instalLar el servidor web apache2 (httpd)?
dnf install httpd

## 16. Què són els targets en SystemD?. Que vol dir multi-user.target?
Antigament hi havia els runlevels del 0 al 6 al SysV que en passar a SystemD són els Targets. Aquests fitxers porten com extensió .target.
La funció dels targets no és una altra que agrupar una sèrie de service units amb les seves dependències. Per exemple existeix el graphical.target que pot arrencar serveis com el gdm.service, accounts-daemon.service, etc... i ho farà en ordre i amb les seves dependències.

Así por ejemplo, el runlevel 3 de System V corresponde al target multi-user.target en systemd y el runlevel 5 correspondería al target llamado  graphical.target.
En un entorno gráfico operamos con graphical.target activo.
Para poner el sistema en un estado de línea de comando usaremos el multi-user.target y automàticamentee todas las unidades gráfics se detendran.

## 17. Qué és un servei?. Com es crea un servei amb SystemD?. Quina estructura té? A on es creen?
Els serveis son petits scripts que realitzen accions que afecten al sistema. 

Els serveis els crearem a:
/etc/systemd/system/ anomenat webserver.service

Amb el següent format:
```
[Unit]
Description=El meu servidor web

[Service]
ExecStart=/usr/bin/python3 -m http.server
# ExecStart, ens diu quin és el procés amb el que comença el servei. Aquest pot llançar altres processos.

[Install]
WantedBy=multi-user.target
# multi-user.target, indica entorn gràfic 
# WantedBy, indica en quin mode arrencaremt el sistema.
# Aquest servei únicament s'arrenca en entorn gràfic.
```

## 18. Una vegada creat un servei, que hem de fer perque el systemctl el tingui en compte?
Una vegada creat si volem provar si funciona primer caldrà recarregar els serveis:
```
systemctl daemon-reload
```
Podem veure els logs del servei amb journalctl -u webserver. Més endavant tractarem el tema dels logs.

## 19. Com podem comprovar informació relacionada amb la execucio dels serveis? 
Per exemple podem comprovar si el servei de cron està funcionant: 
```
systemctl status crond.service
```
Loaded: Carregat al sistema.  
Active: Està corrent en el sistema.  
Main PID: Id del servei  
Tasks: 1 (limit: 19011)  
CGroup: Es llisten els darrers procesos relacionats amb el sistema.


## 20. Sota systemD, qui és l'encarregat de recolectar i emmagatzemar l'activitat del que va passant al sistema?
journalctl